﻿using System.ComponentModel.DataAnnotations;

namespace DomainApplication.Entities
{
    public class PostDTO
    {
        [Key]
        public int Id { get; set; }

        public string? Content { get; set; }
    }
}
