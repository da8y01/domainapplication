﻿using System.ComponentModel.DataAnnotations;

namespace DomainApplication.Entities
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        public string? Content { get; set; }
        public DateTime CreatedTimestamp { get; set; }
    }
}
